package com.retouch.erdem.folder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import org.springframework.stereotype.Service;

@Service
public class ErdemService {
	
	private List<Folder> folders = new ArrayList<>(Arrays.asList(
				new Folder("spring", "SpringFram", "Spring Desc"),
				new Folder("java", "Core Java", "Java Desc"),
				new Folder("javasc", "JavaScript", "JavaScript Desc")
				));
	
	public List<Folder> getAllFolders() {
		return folders;
	}
	
	public Folder getFolder(String id) {
		return folders.stream().filter(t -> t.getId().equals(id)).findFirst().get();
	}

	public void addFolder(Folder folder) {
		folders.add(folder);
	}

}
