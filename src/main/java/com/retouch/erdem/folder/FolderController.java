package com.retouch.erdem.folder;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FolderController {
	
	@Autowired
	private ErdemService folderService;
	
	@RequestMapping("/folders")
	public List<Folder> getAllTopics() {
		return folderService.getAllFolders();
	}
	
	@RequestMapping("/folders/{id}")
	public Folder getFolder(@PathVariable String id) {
		return folderService.getFolder(id);
	}

	//this is f awesome
	@RequestMapping(method=RequestMethod.POST, value="/folder")
	public void addFolder(@RequestBody Folder folder) {
		folderService.addFolder(folder);
	}

}
