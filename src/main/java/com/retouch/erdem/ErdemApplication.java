package com.retouch.erdem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ErdemApplication {

	public static void main(String[] args) {
		SpringApplication.run(ErdemApplication.class, args);
	}

}
